#UDPServer.py
# -*- coding: utf-8 -*-
import json
from flask import Flask ,render_template
from flask_socketio import SocketIO,send, emit
import socket
from time import sleep
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///example.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
class User(db.Model):
    __tablename__ = 'data'
    id = db.Column(db.Integer, primary_key=True)
    data=db.Column(db.Integer,unique=False)
    def __init__(self,data):
        self.data=data
    def __repr__(self):
        return '<User %r>' % self.data
socketio = SocketIO(app)
#建立socket物件
address = ('127.0.0.1', 3150) 
s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

'''
socket套街的格式:1.套接族2.套接字類型3.協議編號(默認為0)
參數一：地址族
	socket.AF_INET IPv4（默認）AF_INET=Address Family Internet
	socket.AF_INET6 
	IPv6socket.AF_UNIX 只能夠用於單一的Unix系統進程間通信
參數二：類型
	　socket.SOCK_STREAM　　流式socket , for TCP （默認）
	　socket.SOCK_DGRAM　　 數據報式socket , for UDP
	　socket.SOCK_RAW 原始套接字，普通的套接字無法處理ICMP、IGMP等網絡報文，而SOCK_RAW可以
'''
s.bind(address)#繫結IP和埠號
@app.route('/')
def index():
    return render_template('demo.html')
@socketio.on('connect')
def handle_my_connect():
#通訊階段  UDP不需要調用listen()方法，而可以直接接收客戶端數據。recvfrom()方法返回資料和客户端的地址與埠號，這樣，伺服器收到資料後，直接調用sendto()就可以把資料用UDP發送客户端。
    data,address= s.recvfrom(1024)
    print (address,data)
    u = User(int(data))
    db.session.add(u)
    db.session.commit()    
    sleep(1)
#關閉連線
		#s.close()
    value=int(data.decode())
    emit('message',{'message': value})
    
@socketio.on('communication')
def handle_communication(msg):
    data,address= s.recvfrom(1024)
    print (address,data)
    u = User(int(data))
    db.session.add(u)
    db.session.commit()
    sleep(1)
    value=int(data.decode())
    emit('message',{'message': value})

if __name__ == '__main__':
    socketio.run(app,host='127.0.0.1',port=3150)
